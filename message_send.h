#ifndef __MESSAGE_SEND_H__
#define __MESSAGE_SEND_H__

#include "message.h"

//Added extern C code for Arduino Compilationss


extern volatile uint8_t tx_mask;
#ifdef __cplusplus
//////////////////////////////////
extern "C" {
#endif
    
uint8_t message_send(const message_t *);

#ifdef __cplusplus
}
#endif
#endif//__MESSAGE_SEND_H__
