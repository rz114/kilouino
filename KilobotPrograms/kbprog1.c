{\rtf1\ansi\ansicpg1252\cocoartf1561\cocoasubrtf600
{\fonttbl\f0\fmodern\fcharset0 CourierNewPSMT;}
{\colortbl;\red255\green255\blue255;\red53\green55\blue68;}
{\*\expandedcolortbl;;\csgenericrgb\c20784\c21569\c26667;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\deftab720
\pard\pardeftab720\ri0\partightenfactor0

\f0\fs20 \cf2 #include <kilolib.h>\
\
// Flag to keep track of new messages.\
int new_message = 0;\
message_t rcvd_message;\
void setup()\
\{\
\}\
\
void loop()\
\{\
	// Blink the LED yellow whenever a message is received.\
	if (new_message == 1)\
	\{\
    	// Reset the flag so the LED is only blinked once per message.\
    	new_message = 0;\
  	 \
      set_color(RGB(0, 0, 1));\
    	delay(100);\
    	set_color(RGB(0, 0, 0));\
   	 \
     	// Spinup the motors to overcome friction.\
	spinup_motors();\
	// Move straight for 2 seconds (1000 ms).\
	set_motors(kilo_straight_left, kilo_straight_right);\
	delay(1000);\
    	set_motors(0, 0);\
	\}\
\}\
\
void message_rx(message_t *message, distance_measurement_t *distance)\
\{\
    \
	rcvd_message = *message;  //store the incoming message\
	// Set the flag on message reception.\
	if(rcvd_message.data[0] == 3) \{\
	new_message = 1;\
	\}\
\}\
\
int main()\
\{\
	kilo_init();\
	// Register the message_rx callback function.\
	kilo_message_rx = message_rx;\
	kilo_start(setup, loop);\
    \
	return 0;\
\}\
}