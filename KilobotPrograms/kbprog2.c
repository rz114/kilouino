{\rtf1\ansi\ansicpg1252\cocoartf1561\cocoasubrtf600
{\fonttbl\f0\fmodern\fcharset0 CourierNewPSMT;\f1\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;\red53\green55\blue68;}
{\*\expandedcolortbl;;\csgenericrgb\c20784\c21569\c26667;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\deftab720
\pard\pardeftab720\ri0\sl312\slmult1\partightenfactor0

\f0\fs20 \cf2 #include <kilolib.h>\
\
#define STOP 0\
#define FORWARD 1\
#define LEFT 2\
#define RIGHT 3\
\
int CLOSE_ENOUGH = 60;\
int current_motion = STOP;\
int new_message = 0;\
int distance = 0;\
int odd = 0;\
int finished = 0;\
\
void setup() \{\
	// put your setup code here, to be run only once\
\}\
\
// Function to handle motion.\
void set_motion(int new_motion)\
\{\
	// Only take an action if the motion is being changed.\
	if (current_motion != new_motion)\
	\{\
    	current_motion = new_motion;\
   	 \
    	if (current_motion == STOP)\
    	\{\
        	set_motors(0, 0);\
    	\}\
    	else if (current_motion == FORWARD)\
    	\{\
        	spinup_motors();\
        	set_motors(kilo_straight_left, kilo_straight_right);\
    	\}\
    	else if (current_motion == LEFT)\
    	\{\
        	spinup_motors();\
        	set_motors(kilo_turn_left, 0);\
    	\}\
    	else if (current_motion == RIGHT)\
    	\{\
        	spinup_motors();\
        	set_motors(0, kilo_turn_right);\
    	\}\
   \}\
\}\
\
void loop() \{\
	// put your main code here, to be run repeatedly\
    \
	// check message\
	if (new_message == 1)\
	\{\
    	if (distance <= CLOSE_ENOUGH)\
    	\{\
        	if (odd == 1)\
        	\{\
            	set_color(RGB(1, 0, 0));\
        	\}\
        	else\
        	\{\
            	set_color(RGB(0, 0, 1));\
        	\}\
        	set_motion(STOP);\
    	\}\
    	else\
    	\{\
        	set_motion(FORWARD); // move forward if close to the beacon\
        	set_color(RGB(1, 1, 0));\
        	new_message = 0;\
    	\}\
	\}\
\
	if (distance > CLOSE_ENOUGH)\
	\{\
    	int random_number = 0;\
    	int dice = 0; // four-sided dice\
    	random_number = rand_hard();\
    	dice = (random_number % 3) + 1; // either move fwd, left, or right, never stop\
    	set_motion(dice);\
    	set_color(RGB(0, 1, 0));\
    	delay(1500);\
	\}\
\}\
\
void message_rx(message_t *message, distance_measurement_t *distance_measurement)\
\{\
	// Set flag on message reception.\
	new_message = 1;\
	distance = estimate_distance(distance_measurement);\
	odd = message->data[0];\
\}\
\
int main() \{\
	// initialize hardware\
	kilo_init();\
	kilo_message_rx = message_rx;\
	// start program\
	kilo_start(setup, loop);\
\
	return 0;\
\}
\f1\fs22 \
}