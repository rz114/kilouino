{\rtf1\ansi\ansicpg1252\cocoartf1561\cocoasubrtf600
{\fonttbl\f0\fmodern\fcharset0 CourierNewPSMT;}
{\colortbl;\red255\green255\blue255;\red53\green55\blue68;}
{\*\expandedcolortbl;;\csgenericrgb\c20784\c21569\c26667;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\deftab720
\pard\pardeftab720\ri0\partightenfactor0

\f0\fs20 \cf2 #include <kilolib.h>\
\
message_t message;\
// Flag to keep track of message transmission.\
int message_sent = 0;\
\
void setup()\
\{\
	// Initialize message:\
	// Message type\
	message.type = NORMAL;\
	// Some data to be sent\
  //  message.data[0] = 1;\
	// CRC Computation\
	message.crc = message_crc(&message);\
\}\
\
void loop()\
\{\
	// Blink the LED magenta whenever a message is sent.\
	if (message_sent == 1)\
	\{\
    	// Reset the flag so the LED is only blinked once per message.\
    	message_sent = 0;\
   	 \
    	set_color(RGB(1, 0, 1));\
    	delay(100);\
    	set_color(RGB(0, 0, 0));\
	\}\
\}\
\
message_t *message_tx()\
\{\
	return &message;\
\}\
\
void message_tx_success()\
\{\
	// Set the flag on message transmission.\
	message_sent = 1;\
\}\
\
int main()\
\{\
	kilo_init();\
	// Register the message_tx callback function.\
	kilo_message_tx = message_tx;\
	// Register the message_tx_success callback function.\
	kilo_message_tx_success = message_tx_success;\
	kilo_start(setup, loop);\
    \
	return 0;\
\}\
}