
#include <avr/io.h>         // io port and addresses
#include <avr/wdt.h>        // watch dog timer
#include <avr/interrupt.h>  // interrupt handling
#include <avr/eeprom.h>     // read eeprom values
#include <avr/sleep.h>      // enter powersaving sleep mode
#include <util/delay.h>     // delay macros
#include <stdlib.h>         // for rand()

#include "kilolib.h"
#include "message_send.h"
#include "macros.h"
#include "ohc.h"
uint16_t tx_clock;                 // number of timer cycles we have waited
uint16_t tx_increment;             // number of timer cycles until next interrupt
volatile uint32_t kilo_ticks;      // internal clock (updated in tx ISR)
message_t message;

#define ir_port PORTB
#define ir_ddr DDRB
#define ir_mask (1<<1)
#define led_port PORTB
#define led_ddr DDRB
#define led_mask (1<<5)

void setup() {
  ir_ddr |= ir_mask;
    led_ddr |= led_mask;
    // Turn off all leds
    led_port &= ~led_mask;
    ir_port &= ~ir_mask;
    // Initialize message:
    // The type is always NORMAL.
   
      // kilo_message_tx = message_tx;
    // Register the message_tx_success callback function.
 ACSR |= (1<<ACD);

    CLKPR = (1<<CLKPCE);
    CLKPR = 1;
    
  // put your setup code here, to run once:
      kilo_message_tx = message_tx;
    kilo_message_tx_success = message_tx_success;

    message.type = NORMAL;

}

void loop() {
  // put your main code here, to run repeatedly:
    tx_clock += tx_increment;
    tx_increment = 0xFF;
    OCR0A = tx_increment;
    kilo_ticks++;

    message.data[0] = 2;
    // CRC
    message.crc = message_crc(&message);
    
    message_t *msg = kilo_message_tx();
        if (msg) {
            if (message_send(msg)) {
                kilo_message_tx_success();
                tx_clock = 0;
            } else {
                tx_increment = rand()&0xFF;
                OCR0A = tx_increment;
            }
        }

    delay(500);
    message.data[0] = 3;
    //CRC
    message.crc = message_crc(&message);
    
        if (msg) {
            if (message_send(msg)) {
                kilo_message_tx_success();
                tx_clock = 0;
            } else {
                tx_increment = rand()&0xFF;
                OCR0A = tx_increment;
            }
        }
    delay(500);


           message.data[0] = 5;
    // CRC
    message.crc = message_crc(&message);
    
        if (msg) {
            if (message_send(msg)) {
                kilo_message_tx_success();
                tx_clock = 0;
            } else {
                tx_increment = rand()&0xFF;
                OCR0A = tx_increment;
            }
        }
      delay(500);          
}

message_t *message_tx()
{
    return &message;
}

void message_tx_success()
{

}
