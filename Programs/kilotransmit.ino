/* Minimal Arduino program to transmit KiloBeacon
 * Author: Raina Zakir, extracts from 
 * Kilobot Library v2 ported for Arduino
 */
//minimal pogram from the library to send a command kilobot
#include "ohc.h"           // for message definitions
//#include "message_crc.h"
#include "message_send.h"
#include "bootldr.h"
#include <util/crc16.h>  // for optimized crc routines
#include "message.h"

message_t message;
// Flag to keep track of message transmission.
int message_sent = 0;
uint16_t tx_clock;                 // number of timer cycles we have waited
uint16_t tx_increment;             // number of timer cycles until next interrupt
volatile uint32_t kilo_ticks;      // internal clock (updated in tx ISR)
#define ir_port PORTB
#define ir_ddr DDRB
#define ir_mask (1<<1)
#define led_port PORTB
#define led_ddr DDRB
#define led_mask (1<<5)
uint8_t *rawmsg;
uint8_t new_packet[PACKET_SIZE];

//Message Redundancy Check function
uint16_t message_crc(const message_t *msg) {
    uint8_t i;
    const uint8_t *rawmsg = (const uint8_t*)msg;
    uint16_t crc = 0xFFFF;
    for (i = 0; i<sizeof(message_t)-sizeof(msg->crc); i++)
        crc = _crc_ccitt_update(crc, rawmsg[i]);
    return crc;
}


void setup(){
   ir_ddr |= ir_mask;
    led_ddr |= led_mask;
    // Turn off all leds
    led_port &= ~led_mask;
    ir_port &= ~ir_mask;
    // Initialize message:
    // The type is always NORMAL.
  
    // kilo_message_tx = message_tx;
    ACSR |= (1<<ACD);

    CLKPR = (1<<CLKPCE);
    CLKPR = 1;
    
    rawmsg = (uint8_t*)&message;


}

void loop(){
    // Blink the LED magenta whenever a message is sent.
    message.type = NORMAL;
    //Red Colour Beacon is 1
    message.data[0] = 1;

   // uint8_t j; 
     //          for (j = 0; j<sizeof(message_t)-sizeof(message.crc); j++){
       //             rawmsg[j] = new_packet[j+2];
         //      } 

    message.crc = message_crc(&message);
        message_send(&message);
        // Reset the flag so the LED is only blinked once per message.
         led_port |= led_mask;
         _delay_ms(150);
         led_port &= ~led_mask;
         _delay_ms(150);
    return 0;

    
}
